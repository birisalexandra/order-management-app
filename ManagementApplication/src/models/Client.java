package models;

public class Client {
	private int id;
	private String name;
	private String address;
	private int age;
	
	/**
	 * Constructor 
	 * Id of the client will be automatically generated
	 * @param name Name of the client
	 * @param address Address of the client
	 * @param age Age of the client
	 */
	public Client(String name, String address, int age) {
		this.name = name;
		this.address = address;
		this.age = age;
	}
	
	public Client(int id, String name, String address, int age) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.age = age;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
}
