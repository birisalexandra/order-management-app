package models;

public class Orders {
	private int id;
	private int quantity;
	private String nameClient;
	private String nameProduct;
	
	public Orders(int id, int quantity, String nameClient, String nameProduct) {
		this.id = id;
		this.quantity = quantity;
		this.nameClient = nameClient;
		this.nameProduct = nameProduct;
	}
	
	public Orders(int quantity, String nameClient, String nameProduct) {
		this.quantity = quantity;
		this.nameClient = nameClient;
		this.nameProduct = nameProduct;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getNameClient() {
		return nameClient;
	}

	public void setNameClient(String nameClient) {
		this.nameClient = nameClient;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}
}
