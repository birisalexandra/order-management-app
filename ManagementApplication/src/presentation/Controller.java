package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import bll.ClientBLL;
import bll.ProductBLL;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import models.Client;
import models.Orders;
import models.Product;

public class Controller {
	private GUI g;

	public Controller(GUI g) {
		this.g = g;
		setComboBox();
		this.g.addNewClientListener(new NewClient());
		this.g.addViewClientsListener(new ViewClients());
		this.g.addComboBoxListener(new Selecter());
		this.g.addDeleteClientListener(new DeleteClient());
		this.g.addEditClientListener(new EditClient());
		setComboBox1();
		this.g.addNewProductListener(new NewProduct());
		this.g.addViewProductListener(new ViewProducts());
		this.g.addComboBox1Listener(new Selecter1());
		this.g.addDeleteProductListener(new DeleteProduct());
		this.g.addEditProductListener(new EditProduct());
		this.g.addOrderListener(new NewOrder());
		this.g.addBillListener(new Bill());
	}

	public class NewClient implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Client client = new Client(g.getTextFieldName(), g.getTextFieldAddress(), g.getTextFieldAge());
			ClientBLL clientBll = new ClientBLL();
			if (clientBll.insertClient(client) < 0)
				g.setlblClient("Age limit is not respected!");
			else
				g.setlblClient("Client succesfully added!");
			g.getComboBox().addItem(client.getName());
			g.getComboBox2().addItem(client.getName());
		}
	}

	public class ViewClients implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			g.updatezTable(getData());
		}
	}
	
	public class Selecter implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ClientDAO clientDAO = new ClientDAO();
			Client c = clientDAO.findByName((String)g.getComboBox().getSelectedItem());
			g.setTextFieldName(c.getName());
			g.setTextFieldAddress(c.getAddress());
			g.setTextFieldAge(c.getAge());
		}
	}
	
	public class DeleteClient implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ClientDAO clientDAO = new ClientDAO();
			String name = g.getTextFieldName();
			clientDAO.delete(name);
			g.setlblClient("Client succesfully deleted!");
			g.getComboBox().removeItem(name);
			g.getComboBox2().removeItem(name);
		}
	}
	
	public class EditClient implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ClientDAO clientDAO = new ClientDAO();
			Client client = new Client(g.getTextFieldName(), g.getTextFieldAddress(), g.getTextFieldAge());
			clientDAO.edit(client, (String)g.getComboBox().getSelectedItem());
			g.setlblClient("Client succesfully edited!");
		}
	}

	private Object[][] getData() {
		ClientDAO clientDAO = new ClientDAO();
		ArrayList<Client> list = clientDAO.view();
		Object[][] date = new Object[list.size()][4];
		Client c = null;
		for (int i = 0; i < list.size(); i++) {
			c = list.get(i);
			date[i][0] = c.getId();
			date[i][1] = c.getName();
			date[i][2] = c.getAddress();
			date[i][3] = c.getAge();
		}
		return date;
	}
	
	private void setComboBox() {
		ClientDAO clientDAO = new ClientDAO();
		ArrayList<Client> list = clientDAO.view();
		Object[][] date = new Object[list.size()][4];
		Client c = null;
		for (int i = 0; i < list.size(); i++) {
			c = list.get(i);
			g.getComboBox().addItem(c.getName());
			g.getComboBox2().addItem(c.getName());
		}
	}
	
	public class NewProduct implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Product product = new Product(g.getTextFieldNameProduct(), g.getTextFieldAmount(), g.getTextFieldPrice());
			ProductDAO productDAO = new ProductDAO();
			productDAO.insert(product);
			g.setlblProduct("Product succesfully added!");
			g.getComboBox1().addItem(product.getName());
			g.getComboBox3().addItem(product.getName());
		}
	}

	public class ViewProducts implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			g.updatezTable1(getData1());
		}
	}
	
	public class Selecter1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ProductDAO productDAO = new ProductDAO();
			Product c = productDAO.findByName((String)g.getComboBox1().getSelectedItem());
			g.setTextFieldNameProduct(c.getName());
			g.setTextFieldAmount(c.getAmount());
			g.setTextFieldPrice(c.getPrice());
		}
	}
	
	public class DeleteProduct implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ProductDAO productDAO = new ProductDAO();
			String name = g.getTextFieldNameProduct();
			productDAO.delete(name);
			g.setlblProduct("Product succesfully deleted!");
			g.getComboBox1().removeItem(name);
			g.getComboBox1().removeItem(name);
		}
	}
	
	public class EditProduct implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ProductDAO productDAO = new ProductDAO();
			Product product = new Product(g.getTextFieldNameProduct(), g.getTextFieldAmount(), g.getTextFieldPrice());
			productDAO.edit(product);
			g.setlblProduct("Product succesfully edited!");
		}
	}

	private Object[][] getData1() {
		ProductDAO productDAO = new ProductDAO();
		ArrayList<Product> list = productDAO.view();
		Object[][] date = new Object[list.size()][4];
		Product c = null;
		for (int i = 0; i < list.size(); i++) {
			c = list.get(i);
			date[i][0] = c.getId();
			date[i][1] = c.getName();
			date[i][2] = c.getAmount();
			date[i][3] = c.getPrice();
		}
		return date;
	}
	
	private void setComboBox1() {
		ProductDAO productDAO = new ProductDAO();
		ArrayList<Product> list = productDAO.view();
		Object[][] date = new Object[list.size()][4];
		Product c = null;
		for (int i = 0; i < list.size(); i++) {
			c = list.get(i);
			g.getComboBox1().addItem(c.getName());
			g.getComboBox3().addItem(c.getName());
		}
	}
	
	public class NewOrder implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			OrderDAO orderDAO = new OrderDAO();
			ProductDAO productDAO = new ProductDAO();
			ProductBLL productBLL = new ProductBLL();
			Product prod1 = productDAO.findByName((String)g.getComboBox3().getSelectedItem()); 
			Product prod2 = new Product(prod1.getName(), prod1.getAmount() - g.getTextFieldQuantity(), prod1.getPrice());
			if (productBLL.orderProduct(prod2) < 0) 
				g.setlblOrder("Not enough stock!");
			else {
				Orders ord = new Orders(g.getTextFieldQuantity(), (String)g.getComboBox2().getSelectedItem(), (String)g.getComboBox3().getSelectedItem());
				orderDAO.insert(ord);
				g.setlblOrder("Order done!");
			}
		}
	}
	
	public class Bill implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			PDF doc = new PDF();
			OrderDAO order = new OrderDAO();
			ProductDAO product = new ProductDAO();
			doc.createPDF(order.extractLast(), product.findByName((String)g.getComboBox3().getSelectedItem()));
		}
	}
}
