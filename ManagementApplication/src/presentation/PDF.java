package presentation;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import models.Orders;
import models.Product;

public class PDF {

	/**
	 * Method used to create the bill into .pdf format and open it automatically
	 * @param ord
	 * @param prod
	 */
	public void createPDF(Orders ord, Product prod) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Bill.pdf"));
			document.open();
			document.addTitle("Bill");
			document.add(new Paragraph("Bill\n\n" + "Bill #id: " + ord.getId() + "\nClient name: " + ord.getNameClient() + "\nProduct name: " + ord.getNameProduct() + "\nQuantity: " + ord.getQuantity() 
			+ "\nPrice per unit: " + prod.getPrice()));
			document.add(new Paragraph("TOTAL: " + ord.getQuantity() * prod.getPrice() + " RON"));
			document.close();
			try {
		        File myFile = new File("C:/Users/biris_000/workspace/ManagementApplication/Bill.pdf");
		        Desktop.getDesktop().open(myFile);
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
