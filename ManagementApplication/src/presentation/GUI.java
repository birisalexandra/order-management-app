package presentation;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;
import javax.swing.JComboBox;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JPanel panelMenu = new JPanel();
	private JTable table =  new JTable();
	private JTextField textFieldName = new JTextField();
	private JTextField textFieldAddress = new JTextField();
	private JTextField textFieldAge = new JTextField();
	private JButton btnClient = new JButton("Client");
	private JButton btnProduct = new JButton("Product");
	private JPanel panelClient = new JPanel();
	private JButton btnViewClients = new JButton("View clients");
	private JButton btnNewClient = new JButton("New client");
	private JButton btnEditClient = new JButton("Edit client");
	private JButton btnDeleteClient = new JButton("Delete client");
	private JButton btnBack = new JButton("Back");
	private JPanel panelProduct = new JPanel();
	private JLabel lblClient = new JLabel("");
	private final JScrollPane scrollPane = new JScrollPane();
	private final JComboBox comboBox = new JComboBox();
	private JComboBox comboBox1 = new JComboBox();
	private JTable table1;
	private JTextField textFieldNameProduct;
	private JTextField textFieldAmount;
	private JTextField textFieldPrice;
	private JButton btnViewProducts = new JButton("View products");
	private JButton btnNewProduct = new JButton("New product");
	private JButton btnEditProduct = new JButton("Edit product");
	private JButton btnNewButton = new JButton("Delete product");
	private JLabel label = new JLabel("");
	private JScrollPane scrollPane_1 = new JScrollPane();
	private final JPanel panelOrder = new JPanel();
	private JTextField textFieldOrder =  new JTextField();
	private JComboBox<String> comboBox2 = new JComboBox();
	private JComboBox<String> comboBox3 = new JComboBox();
	private JButton btnCreateBill = new JButton("Create bill");
	private JLabel labelOrder = new JLabel("");
	private JButton btnOrder_1 = new JButton("Order");
	
	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 517, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		contentPane.add(panelMenu, "name_75667603776957");
		panelMenu.setLayout(null);
		panelMenu.setVisible(true);
		
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelClient.setVisible(true);
				panelMenu.setVisible(false);
			}
		});
		
		btnClient.setBounds(88, 104, 144, 37);
		panelMenu.add(btnClient);
		btnProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelProduct.setVisible(true);
				panelMenu.setVisible(false);
			}
		});
		
		btnProduct.setBounds(262, 104, 144, 37);
		panelMenu.add(btnProduct);
		
		JButton btnOrder = new JButton("Order");
		btnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelOrder.setVisible(true);
				panelMenu.setVisible(false);
			}
		});
		btnOrder.setBounds(180, 152, 144, 37);
		panelMenu.add(btnOrder);
		
		contentPane.add(panelClient, "name_75669523019564");
		panelClient.setLayout(null);
		panelClient.setVisible(false);
		scrollPane.setBounds(10, 11, 294, 185);
		
		panelClient.add(scrollPane);
		scrollPane.setViewportView(table);
		
		JLabel lblName = new JLabel("Name\r\n");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblName.setBounds(325, 81, 70, 20);
		panelClient.add(lblName);
		
		textFieldName.setBounds(370, 82, 121, 20);
		panelClient.add(textFieldName);
		textFieldName.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAddress.setBounds(314, 113, 51, 18);
		panelClient.add(lblAddress);
		
		textFieldAddress.setBounds(370, 113, 121, 20);
		panelClient.add(textFieldAddress);
		textFieldAddress.setColumns(10);
		
		JLabel lblAge = new JLabel("  Age");
		lblAge.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAge.setBounds(324, 142, 51, 18);
		panelClient.add(lblAge);
		
		textFieldAge.setBounds(370, 144, 121, 20);
		panelClient.add(textFieldAge);
		textFieldAge.setColumns(10);
		
		btnViewClients.setBounds(10, 217, 106, 23);
		panelClient.add(btnViewClients);
		
		btnNewClient.setBounds(136, 217, 96, 23);
		panelClient.add(btnNewClient);
		
		btnEditClient.setBounds(253, 217, 96, 23);
		panelClient.add(btnEditClient);
		
		btnDeleteClient.setBounds(376, 217, 105, 23);
		panelClient.add(btnDeleteClient);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelClient.setVisible(false);
				panelMenu.setVisible(true);
			}
		});
		
		btnBack.setBounds(203, 251, 89, 23);
		panelClient.add(btnBack);
		lblClient.setBounds(310, 175, 181, 21);
		
		panelClient.add(lblClient);
		comboBox.setBounds(325, 30, 156, 20);
		
		panelClient.add(comboBox);
		
		contentPane.add(panelProduct, "name_75671407796224");
		panelProduct.setLayout(null);
		
		scrollPane_1.setBounds(10, 11, 301, 194);
		panelProduct.add(scrollPane_1);
		
		table1 = new JTable();
		scrollPane_1.setViewportView(table1);
		
		comboBox1.setBounds(326, 30, 155, 20);
		panelProduct.add(comboBox1);
		
		textFieldNameProduct = new JTextField();
		textFieldNameProduct.setBounds(387, 87, 104, 20);
		panelProduct.add(textFieldNameProduct);
		textFieldNameProduct.setColumns(10);
		
		textFieldAmount = new JTextField();
		textFieldAmount.setBounds(387, 118, 104, 20);
		panelProduct.add(textFieldAmount);
		textFieldAmount.setColumns(10);
		
		textFieldPrice = new JTextField();
		textFieldPrice.setBounds(387, 149, 104, 20);
		panelProduct.add(textFieldPrice);
		textFieldPrice.setColumns(10);
		
		JLabel lblName_1 = new JLabel("Name");
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblName_1.setBounds(341, 88, 46, 17);
		panelProduct.add(lblName_1);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAmount.setBounds(331, 120, 46, 14);
		panelProduct.add(lblAmount);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPrice.setBounds(341, 151, 46, 14);
		panelProduct.add(lblPrice);
		
		btnViewProducts.setBounds(0, 216, 121, 23);
		panelProduct.add(btnViewProducts);
		
		btnNewProduct.setBounds(131, 216, 114, 23);
		panelProduct.add(btnNewProduct);
	
		btnEditProduct.setBounds(248, 216, 114, 23);
		panelProduct.add(btnEditProduct);
		
		btnNewButton.setBounds(362, 216, 129, 23);
		panelProduct.add(btnNewButton);
		
		JButton btnBack1 = new JButton("Back");
		btnBack1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelMenu.setVisible(true);
				panelProduct.setVisible(false);
			}
		});
		btnBack1.setBounds(199, 250, 89, 23);
		panelProduct.add(btnBack1);
		label.setBounds(321, 180, 170, 20);
		
		panelProduct.add(label);
		
		contentPane.add(panelOrder, "name_306678725249611");
		panelOrder.setLayout(null);
		
		JLabel lblSelectClient = new JLabel("Select client");
		lblSelectClient.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSelectClient.setBounds(22, 53, 101, 14);
		panelOrder.add(lblSelectClient);
		
		JLabel lblSelectProduct = new JLabel("Select product");
		lblSelectProduct.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSelectProduct.setBounds(22, 90, 101, 14);
		panelOrder.add(lblSelectProduct);
		
		comboBox2.setBounds(111, 51, 101, 20);
		panelOrder.add(comboBox2);
		
		comboBox3.setBounds(111, 88, 101, 20);
		panelOrder.add(comboBox3);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblQuantity.setBounds(32, 125, 78, 14);
		panelOrder.add(lblQuantity);
		
		textFieldOrder.setBounds(109, 123, 43, 20);
		panelOrder.add(textFieldOrder);
		textFieldOrder.setColumns(10);
		
		btnCreateBill.setBounds(60, 218, 107, 23);
		panelOrder.add(btnCreateBill);
		
		labelOrder.setBounds(22, 163, 185, 14);
		panelOrder.add(labelOrder);
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelMenu.setVisible(true);
				panelOrder.setVisible(false);
			}
		});
		btnNewButton_1.setBounds(60, 252, 107, 23);
		panelOrder.add(btnNewButton_1);
		btnOrder_1.setBounds(60, 184, 107, 23);
		
		panelOrder.add(btnOrder_1);
		panelProduct.setVisible(false);
	}
	public void updatezTable(Object[][] lista) {
		table = new JTable(lista, new Object[]{"ID", "Name", "Address", "Age"});
		scrollPane.setViewportView(table);}
	
	public void updatezTable1(Object[][] lista) {
		table1 = new JTable(lista, new Object[]{"ID", "Name", "Amount", "Price"});
		scrollPane_1.setViewportView(table1);}
	
	public String getTextFieldName() {
		return textFieldName.getText();}
	public String getTextFieldAddress() {
		return textFieldAddress.getText();}
	public int getTextFieldAge() {
		return Integer.parseInt(textFieldAge.getText());}
	public void setTextFieldName(String name) {
		textFieldName.setText(name);}
	public void setTextFieldAddress(String address) {
		textFieldAddress.setText(address);}
	public void setTextFieldAge(int age) {
		textFieldAge.setText(String.valueOf(age));}
	public void setlblClient(String s) {
		lblClient.setText(s);}
	public void setTable(JTable table) {
		this.table = table;}
	public JComboBox<String> getComboBox() {
		return comboBox;}
	
	public JComboBox<String> getComboBox1() {
		return comboBox1;}
	public String getTextFieldNameProduct() {
		return textFieldNameProduct.getText();}
	public int getTextFieldAmount() {
		return Integer.parseInt(textFieldAmount.getText());}
	public float getTextFieldPrice() {
		return Float.parseFloat(textFieldPrice.getText());}
	public void setTextFieldNameProduct(String name) {
		textFieldNameProduct.setText(name);}
	public void setTextFieldAmount(int amount) {
		textFieldAmount.setText(String.valueOf(amount));}
	public void setTextFieldPrice(float price) {
		textFieldPrice.setText(String.valueOf(price));}
	public void setlblProduct(String s) {
		label.setText(s);}
	public void setTable1(JTable table) {
		this.table1 = table;}

	public void addNewClientListener(ActionListener a) {
		btnNewClient.addActionListener(a);}
	public void addViewClientsListener(ActionListener a) {
		btnViewClients.addActionListener(a);}
	public void addEditClientListener(ActionListener a) {
		btnEditClient.addActionListener(a);}
	public void addDeleteClientListener(ActionListener a) {
		btnDeleteClient.addActionListener(a);}
	public void addComboBoxListener(ActionListener a) {
		comboBox.addActionListener(a);}
	
	public void addNewProductListener(ActionListener a) {
		btnNewProduct.addActionListener(a);}
	public void addViewProductListener(ActionListener a) {
		btnViewProducts.addActionListener(a);}
	public void addEditProductListener(ActionListener a) {
		btnEditProduct.addActionListener(a);}
	public void addDeleteProductListener(ActionListener a) {
		btnNewButton.addActionListener(a);}
	public void addComboBox1Listener(ActionListener a) {
		comboBox1.addActionListener(a);}
	
	public void addOrderListener(ActionListener a) {
		btnOrder_1.addActionListener(a);}
	public void addBillListener(ActionListener a) {
		btnCreateBill.addActionListener(a);}
	
	public JComboBox<String> getComboBox2() {
		return comboBox2;}
	public JComboBox<String> getComboBox3() {
		return comboBox3;}
	public int getTextFieldQuantity() {
		return Integer.parseInt(textFieldOrder.getText());}
	public void setlblOrder(String s) {
		labelOrder.setText(s);}
}
