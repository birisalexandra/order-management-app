package bll;

import java.util.ArrayList;

import bll.validators.AgeValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import models.Client;

public class ClientBLL {
	
	private ArrayList<Validator<Client>> validators;
	
	/**
	 * Costructor where an Array List of validators is initialized
	 * We add in the created list different type of validators
	 */
	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new AgeValidator());
	}
	
	/**
	 * @param client, the client we want to put through validator to see if it is valid 
	 * @return
	 */
	public int insertClient(Client client) {
		for (Validator<Client> v : validators) 
			if(v.validate(client) < 0) 
				return -1;
		return ClientDAO.insert(client);
	}
}
