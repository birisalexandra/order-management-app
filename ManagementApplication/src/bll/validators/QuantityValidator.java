package bll.validators;

import models.Product;

public class QuantityValidator implements Validator<Product>{

	@Override
	public int validate(Product t) {
		if(t.getAmount() < 0)
			return -1;
		else
			return 0;
	}
}