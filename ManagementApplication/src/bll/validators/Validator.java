package bll.validators;

public interface Validator<T> {
	public int validate(T t);
}
