package bll.validators;

import models.Client;

public class AgeValidator implements Validator<Client>{
	private static final int MIN_AGE = 18;
	private static final int MAX_AGE = 80;

	public int validate(Client t) {

		if (t.getAge() < MIN_AGE || t.getAge() > MAX_AGE) {
			return -1;
		}
		return 0;
	}
}
