package bll;

import java.util.ArrayList;

import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import models.Product;

public class ProductBLL {
	private ArrayList<Validator<Product>> validators;
	
	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
		validators.add(new QuantityValidator());
	}
	
	public int orderProduct(Product product) {
		for (Validator<Product> v : validators) 
			if(v.validate(product) < 0) 
				return -1;
		return ProductDAO.edit(product);
	}
}
