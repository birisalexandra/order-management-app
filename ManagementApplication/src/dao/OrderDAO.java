package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import models.Orders;

public class OrderDAO {
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertNewOrder = "INSERT INTO orders (quantity,nameClient,nameProduct) VALUES (?,?,?)";
	private static final String extractLast = "SELECT * FROM orders ORDER BY id DESC LIMIT 1";
	
	/**
	 * Method used to insert a new record into the table of orders
	 * @param order
	 * @return 
	 */
	public int insert(Orders order) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		int id = -1;
		try {
			statement = conn.prepareStatement(insertNewOrder, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, order.getQuantity());
			statement.setString(2, order.getNameClient());
			statement.setString(3, order.getNameProduct());
			statement.executeUpdate();
			
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next())
				id = rs.getInt(1);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Order can not be inserted!");
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
		return id;
	}
	
	/**
	 * Method used to extract the last record from the table
	 * Used when creating the bill
	 * @return
	 */
	public Orders extractLast() {
		Orders toReturn = null;
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = conn.prepareStatement(extractLast);
			rs = statement.executeQuery();
			rs.next();

			int id = rs.getInt("id");
			int quantity = rs.getInt("quantity");
			String nameC = rs.getString("nameClient");
			String nameP = rs.getString("nameProduct");
			toReturn = new Orders(id, quantity, nameC, nameP);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Order can not be extracted!");
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
		return toReturn;
	}
}
