package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import models.Product;

public class ProductDAO {
	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertNewProduct = "INSERT INTO product (name,amount,price) VALUES (?,?,?)";
	private static final String selectProducts = "SELECT * FROM shopdb.product";
	private static final String deleteProduct = "DELETE FROM shopdb.product WHERE name = ?";
	private static final String editProduct = "UPDATE shopdb.product SET name = ?, amount = ?, price = ? WHERE name = ?";
	private final static String findStatementString = "SELECT * FROM shopdb.product where name = ?";
	
	/**
	 * Method used to insert a new record into the table
	 * @param product
	 * @return the id of the product
	 */
	public static int insert(Product product) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		int id = -1;
		try {
			statement = conn.prepareStatement(insertNewProduct, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, product.getName());
			statement.setInt(2, product.getAmount());
			statement.setFloat(3, product.getPrice());
			statement.executeUpdate();
			
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next())
				id = rs.getInt(1);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Product can not be inserted!");
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
		return id;
	}
	
	/**
	 * Method used to find a product in the database by the name
	 * @param productName
	 * @return
	 */
	public Product findByName(String productName) {
		Product toReturn = null;

		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = conn.prepareStatement(findStatementString);
			statement.setString(1, productName);
			rs = statement.executeQuery();
			rs.next();

			int id = rs.getInt("id");
			int amount = rs.getInt("amount");
			float price = rs.getFloat("price");
			toReturn = new Product(id, productName, amount, price);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
		return toReturn;
	}
	
	public void delete(String productName) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = conn.prepareStatement(deleteProduct);
			statement.setString(1, productName);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Product can not be inserted!");
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
	}
	
	public static int edit(Product product) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		int returned = -1;
		try {
			statement = conn.prepareStatement(editProduct);
			statement.setString(1, product.getName());
			statement.setInt(2, product.getAmount());
			statement.setFloat(3, product.getPrice());
			statement.setString(4, product.getName());
			statement.execute();
			returned = 0;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Product can not be inserted!");
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
		return returned;
	}
	
	public ArrayList<Product> view() {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		ArrayList<Product> list = new ArrayList<Product>();
		try {
			statement = conn.prepareStatement(selectProducts);
			rs =  statement.executeQuery();
			while(rs.next()) {
				Product product = new Product(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getFloat(4));
				list.add(product);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Clients can not be listed!");
		} finally {
			ConnectionFactory.close(conn);
			ConnectionFactory.close(statement);
		}		
		return list;
	}
}
