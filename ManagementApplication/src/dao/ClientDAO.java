package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import models.Client;

public class ClientDAO {
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertNewClient = "INSERT INTO client (name,address,age) VALUES (?,?,?)";
	private static final String selectClients = "SELECT * FROM shopdb.client";
	private static final String deleteClient = "DELETE FROM shopdb.client WHERE name = ?";
	private static final String editClient = "UPDATE shopdb.client SET name = ?, address = ?, age = ? WHERE name = ?";
	private final static String findStatementString = "SELECT * FROM shopdb.client where name = ?";
	
	/**
	 * Insert a new record into the database
	 * @param client, the new client we want to insert
	 * @return 
	 */
	public static int insert(Client client) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		int id = -1;
		try {
			statement = conn.prepareStatement(insertNewClient, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, client.getName());
			statement.setString(2, client.getAddress());
			statement.setInt(3, client.getAge());
			statement.executeUpdate();
			
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next())
				id = rs.getInt(1);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Client can not be inserted!");
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
		return id;
	}
	
	/**
	 * Find a record from the database
	 * @param clientName, the name of the client we want to find
	 * @return the found Client
	 */
	public Client findByName(String clientName) {
		Client toReturn = null;

		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = conn.prepareStatement(findStatementString);
			statement.setString(1, clientName);
			rs = statement.executeQuery();
			rs.next();

			int id = rs.getInt("id");
			String address = rs.getString("address");
			int age = rs.getInt("age");
			toReturn = new Client(id, clientName, address, age);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"StudentDAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
		return toReturn;
	}
	
	/**
	 * Delete a record from the database by selecting a name
	 * @param clientName
	 */
	public void delete(String clientName) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = conn.prepareStatement(deleteClient);
			statement.setString(1, clientName);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Client can not be inserted!");
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
	}
	
	/**
	 * Edit a record from the database
	 * @param client, the client we want to edit
	 * @param clientName
	 */
	public void edit(Client client, String clientName) {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		try {
			statement = conn.prepareStatement(editClient);
			statement.setString(1, client.getName());
			statement.setString(2, client.getAddress());
			statement.setInt(3, client.getAge());
			statement.setString(4, clientName);
			statement.execute();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Client can not be inserted!");
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(conn);
		}
	}
	
	/**
	 * Creating an array list of all clients in the database
	 * Used to display the records into a JTable 
	 * @return
	 */
	public ArrayList<Client> view() {
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		ArrayList<Client> list = new ArrayList<Client>();
		try {
			statement = conn.prepareStatement(selectClients);
			rs =  statement.executeQuery();
			while(rs.next()) {
				Client client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
				list.add(client);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Clients can not be listed!");
		} finally {
			ConnectionFactory.close(conn);
			ConnectionFactory.close(statement);
		}		
		return list;
	}
}
