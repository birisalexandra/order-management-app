package start;

import java.util.logging.Logger;

import presentation.Controller;
import presentation.GUI;

public class Main {
	protected static final Logger LOGGER = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) {
		
		GUI g = new GUI();
		Controller con = new Controller(g);
		g.setVisible(true);
	}
}
